package com.philippschumann.dttrealestate.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.philippschumann.dttrealestate.R;
import com.philippschumann.dttrealestate.interfaces.HouseAdapterListener;
import com.philippschumann.dttrealestate.model.House;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class HouseAdapter extends RecyclerView.Adapter<HouseAdapter.ViewHolder> implements Filterable {
    private List<House> items;
    private List<House> itemsFiltered;
    private HouseAdapterListener houseAdapterListener;
    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            //create new list and add matching houses
            String searchText = String.valueOf(charSequence);
            FilterResults filterResults = new FilterResults();
            List<House> filteredList = new ArrayList<>();
            if (searchText.isEmpty()) {
                filteredList.addAll(items);
            } else {
                for (House house : items) {
                    if (house.getFormattedZipCity().toLowerCase().contains(searchText.toLowerCase())) {
                        filteredList.add(house);
                    }
                }
            }
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            itemsFiltered = (ArrayList<House>) filterResults.values;
            houseAdapterListener.onFilterResult(itemsFiltered.size());
            notifyDataSetChanged();
        }
    };

    @NonNull
    @Override
    public HouseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_houses_list_item, parent, false);
        return new ViewHolder(itemView);
    }

    public HouseAdapter(HouseAdapterListener houseAdapterListener) {
        this.houseAdapterListener = houseAdapterListener;
        items = new ArrayList<>();
        itemsFiltered = new ArrayList<>();
    }

    @Override
    public void onBindViewHolder(@NonNull HouseAdapter.ViewHolder holder, final int position) {
        House currentItem = itemsFiltered.get(position);
        holder.textViewPrice.setText(currentItem.getFormattedPrice());
        holder.textViewZipCity.setText(currentItem.getFormattedZipCity());
        //Use String.valueOf() to prevent ResourceNotFoundException
        holder.textViewBedroom.setText(String.valueOf(currentItem.getBedrooms()));
        holder.textViewBathroom.setText(String.valueOf(currentItem.getBathrooms()));
        holder.textViewSize.setText(currentItem.getFormattedSize());
        holder.textViewDistance.setText(currentItem.getFormattedDistance());
        holder.imageViewThumbnail.setImageBitmap(currentItem.getImage());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                houseAdapterListener.onHouseClicked(itemsFiltered.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewPrice;
        public TextView textViewZipCity;
        public TextView textViewBedroom;
        public TextView textViewBathroom;
        public TextView textViewSize;
        public TextView textViewDistance;
        public ImageView imageViewThumbnail;
        public CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewPrice = itemView.findViewById(R.id.tv_price);
            textViewZipCity = itemView.findViewById(R.id.tv_zip_city);
            textViewBedroom = itemView.findViewById(R.id.tv_bed);
            textViewDistance = itemView.findViewById(R.id.tv_distance);
            textViewBathroom = itemView.findViewById(R.id.tv_bath);
            textViewSize = itemView.findViewById(R.id.tv_size);
            imageViewThumbnail = itemView.findViewById(R.id.iv_thumbnail);
            cardView = itemView.findViewById(R.id.card);
        }
    }

    public void setItems(House[] items) {
        this.items = Arrays.asList(items);
        this.itemsFiltered = this.items;
        notifyDataSetChanged();
    }
}
