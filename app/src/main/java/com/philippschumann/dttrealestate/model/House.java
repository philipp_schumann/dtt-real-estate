package com.philippschumann.dttrealestate.model;

import android.graphics.Bitmap;

import com.google.gson.annotations.SerializedName;
import com.philippschumann.dttrealestate.util.FormatHelper;

public class House implements Comparable<House> {

    private int id;
    @SerializedName("image")
    private String imagePath;
    private int price;
    private int bedrooms;
    private int bathrooms;
    private int size;
    private String description;
    private String zip;
    private String city;
    private int latitude;
    private int longitude;
    private String createdDate;
    private double distance = -1.0d;
    @SerializedName("bitmap")
    private Bitmap image;

    //no constructor necessary when creating houses via gson

    @Override
    public int compareTo(House house) {
        return this.price - house.getPrice();
    }

    public int getId() {
        return id;
    }

    public String getImagePath() {
        return imagePath;
    }

    public int getPrice() {
        return price;
    }

    public String getFormattedPrice() {
        return FormatHelper.getInstance().formatCurrency(price);
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public int getSize() {
        return size;
    }

    public String getFormattedSize() {
        return size + " m²";
    }

    public String getDescription() {
        return description;
    }

    public String getZip() {
        return zip;
    }

    public String getCity() {
        return city;
    }

    public String getFormattedZipCity() {
        return zip + " " + city;
    }

    public int getLatitude() {
        return latitude;
    }

    public int getLongitude() {
        return longitude;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getFormattedDistance() {
        if (distance == -1) {
            return "? km";
        } else {
            return String.format("%.1f", distance) + " km";
        }
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}
