package com.philippschumann.dttrealestate;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.bottomnavigation.BottomNavigationView;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int SPLASH_TIME_MILLIS = 1000;
    private BottomNavigationView navView;
    private NavController navController;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "saved instance: " + savedInstanceState);
        if (savedInstanceState == null) {
            HouseRepository.getInstance().requestHouses(getApplicationContext());
            showSplash();
        }
        //leave splash screen by setting default theme
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        navView = findViewById(R.id.nav_view);

        //setSupportActionBar(toolbar);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_houses, R.id.navigation_about)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupWithNavController(toolbar, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        navView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectFragment(item);
                return true;
            }
        });
        printScreenSizeInfo();
    }

    //navigate to fragment and highlight nav bar item
    private void selectFragment(MenuItem item) {
        int currentlySelected = navView.getMenu().findItem(navView.getSelectedItemId()).getItemId();
        switch (item.getItemId()) {
            case R.id.navigation_houses:
                if (currentlySelected == R.id.navigation_about) {
                    navController.navigate(R.id.nav_from_about_to_houses);
                }
                break;
            case R.id.navigation_about:
                if (currentlySelected == R.id.navigation_houses) {
                    navController.navigate(R.id.nav_from_houses_to_about);
                }
                break;
        }

        for (int i = 0; i < navView.getMenu().size(); i++) {
            MenuItem menuItem = navView.getMenu().getItem(i);
            if (menuItem.getItemId() == item.getItemId()) {
                menuItem.setChecked(true);
            }
        }
    }

    private void showSplash() {
        try {
            Thread.sleep(SPLASH_TIME_MILLIS);
            // usually do something instead
            // for example load app resources
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //testing purposes
    private void printScreenSizeInfo() {
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        float density = getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;
        Log.d(TAG, "screen width (dp): " + dpWidth);
    }
}