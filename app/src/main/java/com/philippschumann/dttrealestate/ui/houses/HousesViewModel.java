package com.philippschumann.dttrealestate.ui.houses;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.philippschumann.dttrealestate.HouseRepository;
import com.philippschumann.dttrealestate.interfaces.HouseObserver;
import com.philippschumann.dttrealestate.interfaces.RepoObservable;
import com.philippschumann.dttrealestate.model.House;

//Use AndroidViewModel instead of ViewModel to get application context
public class HousesViewModel extends AndroidViewModel implements HouseObserver {
    private static final String TAG = "HouseVM";
    private MutableLiveData<House[]> houses;
    private double latitude = 0;
    private double longitude = 0;
    private RepoObservable repoObservable;

    public HousesViewModel(@NonNull Application application) {
        super(application);
        houses = new MutableLiveData<>();
        repoObservable = HouseRepository.getInstance();
        repoObservable.registerObserver(this);
    }

    public LiveData<House[]> getHouses() {
        return houses;
    }

    //Calculate distance using the Haversine formula.
    //credits to https://stackoverflow.com/a/16794680
    public static double distance(double lat1, double lon1, double lat2,
                                  double lon2) {

        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);
        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return R * c; // convert to meters
    }

    @Override
    public void update(House[] houses) {
        this.houses.setValue(houses);
        updateDistance();
    }

    @Override
    public void updateImage(int id, Bitmap bitmap) {
        if (houses.getValue() != null) {
            House[] houses = this.houses.getValue();
            for (House house : houses) {
                if (id == house.getId()) {
                    house.setImage(bitmap);
                }
            }
            this.houses.setValue(houses);
        }
    }

    //distance needs to be updated when data is loaded and location is known
    public void updateDistance() {
        Log.d(TAG, "try update distance");
        if (latitude != 0 && longitude != 0 && houses.getValue() != null) {
            Log.d(TAG, "update distance");
            House[] houses = this.houses.getValue();
            for (House house : houses) {
                house.setDistance(distance(latitude, longitude, house.getLatitude(), house.getLongitude()));
            }
            this.houses.setValue(houses);
        }
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        repoObservable.removeObserver(this);
    }
}