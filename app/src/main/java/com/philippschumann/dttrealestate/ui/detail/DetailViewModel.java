package com.philippschumann.dttrealestate.ui.detail;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.philippschumann.dttrealestate.HouseRepository;
import com.philippschumann.dttrealestate.model.House;

public class DetailViewModel extends AndroidViewModel {
    private static final String TAG = DetailViewModel.class.getSimpleName();
    private MutableLiveData<House> house;


    public DetailViewModel(@NonNull Application application) {
        super(application);
        house = new MutableLiveData<>();
    }

    public LiveData<House> getHouse() {
        return house;
    }

    public void setHouseById(int id) {
        house.setValue(HouseRepository.getInstance().getHouseById(id));
    }


}
