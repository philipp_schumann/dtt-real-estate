package com.philippschumann.dttrealestate.ui.houses;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.textfield.TextInputLayout;
import com.philippschumann.dttrealestate.R;
import com.philippschumann.dttrealestate.adapter.HouseAdapter;
import com.philippschumann.dttrealestate.interfaces.HouseAdapterListener;
import com.philippschumann.dttrealestate.model.House;
import com.philippschumann.dttrealestate.ui.detail.DetailActivity;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class HousesFragment extends Fragment implements HouseAdapterListener {
    private static final String TAG = HousesFragment.class.getSimpleName();
    public static final String HOUSE_ID = "house_id";
    private HousesViewModel housesViewModel;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private HouseAdapter houseAdapter;
    private static final int PERMISSION_CODE_ACCESS_LOCATION = 123;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private AppCompatEditText editTextSearch;
    private ConstraintLayout constraintLayoutSearch;
    private int translationYEditTextSearch;
    private LocationCallback locationCallback;
    private InputMethodManager inputMethodManager;
    private TextInputLayout textInputLayoutSearch;
    private TextWatcher editTextSearchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            //apply filter and swap search bar icons depending on text input
            houseAdapter.getFilter().filter(charSequence);
            if (charSequence.length() >= 1) {
                textInputLayoutSearch.setEndIconDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.ic_close));
            } else {
                textInputLayoutSearch.setEndIconDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.ic_search));
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        housesViewModel = new ViewModelProvider(requireActivity()).get(HousesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_houses, container, false);
        recyclerView = root.findViewById(R.id.recyclerview_houses);
        editTextSearch = root.findViewById(R.id.et_search);
        textInputLayoutSearch = root.findViewById(R.id.text_input_layout_search);
        constraintLayoutSearch = root.findViewById(R.id.search_layout);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        houseAdapter = new HouseAdapter(this);
        if (housesViewModel.getHouses().getValue() != null) {
            houseAdapter.setItems(housesViewModel.getHouses().getValue());
        }
        recyclerView.setAdapter(houseAdapter);

        editTextSearch.addTextChangedListener(editTextSearchWatcher);
        textInputLayoutSearch.setHintEnabled(false);
        textInputLayoutSearch.setEndIconMode(TextInputLayout.END_ICON_CUSTOM);
        textInputLayoutSearch.setEndIconDrawable(ContextCompat.getDrawable(requireActivity(), R.drawable.ic_search));
        textInputLayoutSearch.setEndIconOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //clear text in search bar or hide keyboard depending on text input
                if (!TextUtils.isEmpty(editTextSearch.getText())) {
                    editTextSearch.setText("");
                } else {
                    editTextSearch.requestFocus();
                    showKeyboard();
                }
            }
        });

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int maxDistance = constraintLayoutSearch.getHeight();
                //  Log.d(TAG, "scroll offset: " + recyclerView.computeVerticalScrollOffset());
                //  Log.d(TAG, "dy: " + dy);
                //  Log.d(TAG, "max dist: " + maxDistance);

                // cap translation between 0 and maxDistance
                if (dy != 0) {
                    translationYEditTextSearch += dy;
                    if (translationYEditTextSearch > maxDistance) {
                        translationYEditTextSearch = maxDistance;
                    } else if (translationYEditTextSearch < 0) {
                        translationYEditTextSearch = 0;
                    }

                    // apply translation and hide keyboard if text input is empty
                    if (TextUtils.isEmpty(editTextSearch.getText())) {
                        editTextSearch.clearFocus();
                        hideKeyboard();
                        textInputLayoutSearch.setTranslationY(-translationYEditTextSearch);
                    }
                }
            }
        });

        //update recyclerview on houses change
        housesViewModel.getHouses().observe(getViewLifecycleOwner(), new Observer<House[]>() {
            @Override
            public void onChanged(House[] houses) {
                houseAdapter.setItems(houses);
            }
        });

        inputMethodManager = (InputMethodManager) requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Log.d(TAG, "location result: " + locationResult);
                if (locationResult != null) {
                    housesViewModel.setLatitude(locationResult.getLastLocation().getLatitude());
                    housesViewModel.setLongitude(locationResult.getLastLocation().getLongitude());
                    housesViewModel.updateDistance();
                    //disable location updates after retrieving location
                    fusedLocationProviderClient.removeLocationUpdates(locationCallback);
                }
            }
        };

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity());
        handlePermissions();
    }

    @Override
    public void onHouseClicked(House house) {
        hideKeyboard();
        Intent detailIntent = new Intent(requireContext(), DetailActivity.class);
        detailIntent.putExtra(HOUSE_ID, house.getId());
        startActivity(detailIntent);
    }

    @Override
    public void onFilterResult(int length) {
        // show "no search result" view by hiding recyclerview
        if (length <= 0) {
            recyclerView.setVisibility(View.INVISIBLE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void handlePermissions() {
        if ((ContextCompat.checkSelfPermission(requireContext(), ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(requireContext(), ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            //permissions granted
            getLocation();
        } else if (shouldShowRequestPermissionRationale(ACCESS_FINE_LOCATION) || shouldShowRequestPermissionRationale(ACCESS_COARSE_LOCATION)) {
            //display educational UI why the permission is necessary
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(requireActivity());
            alertDialogBuilder.setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, PERMISSION_CODE_ACCESS_LOCATION);
                }
            });
            alertDialogBuilder.setNegativeButton(getString(R.string.no_thanks), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
            alertDialogBuilder.setTitle(getString(R.string.permission_rationale_title));
            alertDialogBuilder.setMessage(getString(R.string.permission_rationale_message));
            alertDialogBuilder.show();

        } else {
            // You can directly ask for the permission.
            requestPermissions(new String[]{ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, PERMISSION_CODE_ACCESS_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_CODE_ACCESS_LOCATION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permission granted
                getLocation();
            }
        }
    }

    private void getLocation() {
        //generated permission check
        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationRequest locationRequest = LocationRequest.create()
                .setInterval(10000)
                .setFastestInterval(5000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        Log.d(TAG, "start location updates");
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
    }


    private void hideKeyboard() {
        inputMethodManager.hideSoftInputFromWindow(requireView().getRootView().getWindowToken(), 0);
    }

    private void showKeyboard() {
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }
}