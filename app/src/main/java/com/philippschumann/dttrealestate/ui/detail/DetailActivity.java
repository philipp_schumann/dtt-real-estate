package com.philippschumann.dttrealestate.ui.detail;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.BlendModeColorFilterCompat;
import androidx.core.graphics.BlendModeCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.philippschumann.dttrealestate.R;
import com.philippschumann.dttrealestate.model.House;
import com.philippschumann.dttrealestate.ui.houses.HousesFragment;
import com.philippschumann.dttrealestate.util.SquareMapView;

import java.util.Objects;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback, AppBarLayout.OnOffsetChangedListener {

    private static final String TAG = DetailActivity.class.getSimpleName();
    private SquareMapView mapView;
    private DetailViewModel detailViewModel;
    private Toolbar toolbar;
    private AppBarLayout appBarLayout;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private TextView textViewPrice;
    private TextView textViewBed;
    private TextView textViewBath;
    private TextView textViewSize;
    private TextView textViewDistance;
    private TextView textViewDescriptionText;
    private TextView textViewLocationText;
    private ImageView imageViewHouse;
    private NestedScrollView scrollView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        toolbar = findViewById(R.id.toolbar);
        appBarLayout = findViewById(R.id.appBar);
        collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar_layout);
        textViewPrice = findViewById(R.id.tv_price);
        textViewBed = findViewById(R.id.tv_bed);
        textViewBath = findViewById(R.id.tv_bath);
        textViewSize = findViewById(R.id.tv_size);
        textViewDistance = findViewById(R.id.tv_distance);
        textViewDescriptionText = findViewById(R.id.tv_description_text);
        textViewLocationText = findViewById(R.id.tv_location_text);
        imageViewHouse = findViewById(R.id.iv_toolbar_image);
        scrollView = findViewById(R.id.scrollView);
        mapView = findViewById(R.id.map);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        //show up arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        appBarLayout.addOnOffsetChangedListener(this);

        Intent intent = getIntent();

        detailViewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        if (intent.hasExtra(HousesFragment.HOUSE_ID)) {
            detailViewModel.setHouseById(intent.getExtras().getInt(HousesFragment.HOUSE_ID));
        }
        detailViewModel.getHouse().observe(this, new Observer<House>() {
            @Override
            public void onChanged(House house) {
                setContent();
            }
        });

        //prevent scrollview scrolling when user interacts with mapview
        mapView.setListener(new SquareMapView.OnTouchListener() {
            @Override
            public void onTouch() {
                scrollView.requestDisallowInterceptTouchEvent(true);
            }
        });
        mapView.onCreate(savedInstanceState);
        //register callback
        mapView.getMapAsync(this);

        setContent();
    }

    private void setContent() {
        if (detailViewModel.getHouse().getValue() != null) {
            textViewPrice.setText(detailViewModel.getHouse().getValue().getFormattedPrice());
            textViewBed.setText(String.valueOf(detailViewModel.getHouse().getValue().getBedrooms()));
            textViewBath.setText(String.valueOf(detailViewModel.getHouse().getValue().getBathrooms()));
            textViewSize.setText(detailViewModel.getHouse().getValue().getFormattedSize());
            textViewDistance.setText(detailViewModel.getHouse().getValue().getFormattedDistance());
            textViewDescriptionText.setText(detailViewModel.getHouse().getValue().getDescription());
            textViewLocationText.setText(detailViewModel.getHouse().getValue().getFormattedZipCity());
            imageViewHouse.setImageBitmap(detailViewModel.getHouse().getValue().getImage());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (detailViewModel.getHouse().getValue() != null) {
            double latitude = detailViewModel.getHouse().getValue().getLatitude();
            double longitude = detailViewModel.getHouse().getValue().getLongitude();
            LatLng latLng = new LatLng(latitude, longitude);
            googleMap.addMarker(new MarkerOptions().position(latLng));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f));
            //googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12f));
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        //change up arrow color depending on scroll state to prevent white arrow on white background
        if ((collapsingToolbarLayout.getHeight() + verticalOffset) < (2 * ViewCompat.getMinimumHeight(collapsingToolbarLayout))) {
            Objects.requireNonNull(toolbar.getNavigationIcon()).setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(this, R.color.dtt_medium), BlendModeCompat.SRC_ATOP));
        } else {
            Objects.requireNonNull(toolbar.getNavigationIcon()).setColorFilter(BlendModeColorFilterCompat.createBlendModeColorFilterCompat(ContextCompat.getColor(this, android.R.color.primary_text_dark), BlendModeCompat.SRC_ATOP));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
