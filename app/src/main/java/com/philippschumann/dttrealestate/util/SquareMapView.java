package com.philippschumann.dttrealestate.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.google.android.gms.maps.MapView;

public class SquareMapView extends MapView {

    private OnTouchListener touchListener;

    public SquareMapView(Context context) {
        super(context);
    }

    public SquareMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SquareMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setListener(OnTouchListener listener) {
        touchListener = listener;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_UP:
                touchListener.onTouch();
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // passing width as height makes the view always as high as it is wide
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    public interface OnTouchListener {
        void onTouch();
    }
}
