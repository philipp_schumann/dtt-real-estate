package com.philippschumann.dttrealestate.util;

import android.icu.text.NumberFormat;

public class FormatHelper {
    private static FormatHelper instance;

    public static FormatHelper getInstance() {

        if (FormatHelper.instance == null) {
            FormatHelper.instance = new FormatHelper();
        }
        return FormatHelper.instance;
    }

    // format example: 1000 -> $ 1,000
    public String formatCurrency(int value) {
        NumberFormat format = NumberFormat.getNumberInstance();
        format.setCurrency(android.icu.util.Currency.getInstance("USD"));
        return "$ " + format.format(value);
    }
}
