package com.philippschumann.dttrealestate;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.philippschumann.dttrealestate.interfaces.HouseObserver;
import com.philippschumann.dttrealestate.interfaces.RepoObservable;
import com.philippschumann.dttrealestate.model.House;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class HouseRepository implements RepoObservable {
    private static final String TAG = HouseRepository.class.getSimpleName();
    private static final String DOMAIN_URL = "https://intern.docker-dev.d-tt.nl";
    private static final Map<String, String> ACCESS_KEY_PARAMS = Collections.singletonMap("Access-Key", "98bww4ezuzfePCYFxJEWyszbUXc7dxRx");
    private static HouseRepository instance;
    private ArrayList<HouseObserver> observers;
    private RequestQueue queue;
    private House[] houses;

    private HouseRepository() {
        observers = new ArrayList<>();
    }

    public static HouseRepository getInstance() {

        if (HouseRepository.instance == null) {
            HouseRepository.instance = new HouseRepository();
        }
        return HouseRepository.instance;
    }

    public void requestHouses(final Context context) {
        queue = Volley.newRequestQueue(context);
        String url = DOMAIN_URL + "/api/house";
        Log.d(TAG, "url: " + url);
        final JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, response.toString());
                Gson gson = new Gson();
                houses = gson.fromJson(response.toString(), House[].class);
                Arrays.sort(houses);
                notifyObservers();
                requestImages();
                Log.d(TAG, "houses: " + Arrays.toString(houses));
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, Objects.requireNonNull(error.getMessage()));
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return ACCESS_KEY_PARAMS;
            }
        };
        queue.add(request);
    }

    private void requestImages() {
        ImageRequest imageRequest;
        for (final House house : houses) {
            imageRequest = new ImageRequest(DOMAIN_URL + house.getImagePath(), new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {
                    Log.d(TAG, "received bitmap: " + house.getId());
                    notifyObserversNewImage(house.getId(), response);
                }
            }, 720, 720, ImageView.ScaleType.CENTER, Bitmap.Config.RGB_565, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, Objects.requireNonNull(error.getMessage()));
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return ACCESS_KEY_PARAMS;
                }
            };
            queue.add(imageRequest);
        }
    }


    @Override
    public void registerObserver(HouseObserver houseObserver) {
        if (!observers.contains(houseObserver)) {
            observers.add(houseObserver);
        }
    }

    @Override
    public void removeObserver(HouseObserver houseObserver) {
        observers.remove(houseObserver);
    }

    @Override
    public void notifyObservers() {
        for (HouseObserver observer : observers) {
            observer.update(houses);
        }
    }

    @Override
    public void notifyObserversNewImage(int id, Bitmap bitmap) {
        for (HouseObserver observer : observers) {
            observer.updateImage(id, bitmap);
        }
    }

    public House getHouseById(int id) {
        for (House house : houses) {
            if (id == house.getId()) {
                return house;
            }
        }
        return null;
    }
}
