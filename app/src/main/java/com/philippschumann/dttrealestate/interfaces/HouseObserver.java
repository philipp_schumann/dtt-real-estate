package com.philippschumann.dttrealestate.interfaces;

import android.graphics.Bitmap;

import com.philippschumann.dttrealestate.model.House;

public interface HouseObserver {
    void update(House[] houses);

    void updateImage(int id, Bitmap bitmap);
}
