package com.philippschumann.dttrealestate.interfaces;

import android.graphics.Bitmap;

public interface RepoObservable {
    void registerObserver(HouseObserver houseObserver);

    void removeObserver(HouseObserver houseObserver);

    void notifyObservers();

    void notifyObserversNewImage(int id, Bitmap bitmap);
}
