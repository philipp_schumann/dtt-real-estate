package com.philippschumann.dttrealestate.interfaces;

import com.philippschumann.dttrealestate.model.House;

public interface HouseAdapterListener {
    void onHouseClicked(House house);

    void onFilterResult(int length);
}
